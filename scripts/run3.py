import logging
from pathlib import Path
import time

import numpy as np
import pytorch_lightning as pl
import torch
import torch.nn.functional as F
from torchvision import transforms, datasets
from tqdm import tqdm

from tonbo.ram_lpm import RAM_LPM
from tonbo.utils import square_pad, AverageMeter


class Net(RAM_LPM):
    def __init__(
        self,
        std=0.16,
        num_classes=1000,
        kernel_sizes_conv2d=[[3, 3]] * 11,
        kernel_sizes_pool=[
            [3, 3],
            [1, 1],
            [1, 1],
            [1, 1],
            [3, 3],
            [1, 1],
            [1, 1],
            [1, 1],
            [1, 2],
            [1, 1],
            [3, 3],
        ],
        strides_pool=[
            [3, 3],
            [1, 1],
            [1, 1],
            [1, 1],
            [3, 3],
            [1, 1],
            [1, 1],
            [1, 1],
            [1, 2],
            [1, 1],
            [3, 3],
        ],
        kernel_dims=[3, 128, 128, 128, 128, 256, 256, 256, 256, 512, 512, 512],
        kernel_sizes_conv2d_where=[[3, 3]] * 3,
        kernel_sizes_pool_where=[[3, 3], [1, 1], [1, 3]],
        strides_pool_where=[[3, 3], [1, 1], [1, 3]],
        kernel_dims_where=[3, 32, 32, 32],
        r_min=0.02,
        r_max=1.0,
        H=54,
        W=108,
        hidden_what=512,
        hidden_where=128,
        upsampling_factor_r=1,
        upsampling_factor_theta=1,
        log_r=True,
        mc_sampling=1,
        num_glimpses=10,
        batch_size=96,
        init_lr_where=1e-6,
        init_lr=1e-4,
        init_lr_baseliner=1e-4,
    ):
        super(Net, self).__init__(
            std,
            num_classes,
            kernel_sizes_conv2d,
            kernel_sizes_pool,
            strides_pool,
            kernel_dims,
            kernel_sizes_conv2d_where,
            kernel_sizes_pool_where,
            strides_pool_where,
            kernel_dims_where,
            r_min,
            r_max,
            H,
            W,
            hidden_what,
            hidden_where,
            upsampling_factor_r,
            upsampling_factor_theta,
            log_r,
            mc_sampling,
            num_glimpses,
        )
        self.imagenet_size = 512
        self.batch_size = batch_size
        self.save_hyperparameters()
        self.init_lr_where = init_lr_where
        self.init_lr = init_lr
        self.init_lr_baseliner = init_lr_baseliner
        self.normalize = transforms.Normalize(
            mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]
        )
        self.num_workers = 4
        self.pin_memory = True
        self.num_glimpses_list = num_glimpses

    def train_dataloader(self):
        transform_train = transforms.Compose(
            [
                square_pad,
                transforms.Resize([self.imagenet_size, self.imagenet_size]),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                self.normalize,
            ]
        )
        train_dataset = datasets.ImageFolder("../train", transform=transform_train)
        train_loader = torch.utils.data.DataLoader(
            train_dataset,
            batch_size=self.batch_size,
            shuffle=True,
            num_workers=self.num_workers,
            pin_memory=self.pin_memory,
        )
        return train_loader

    def val_dataloader(self):
        transform_val = transforms.Compose(
            [
                square_pad,
                transforms.Resize([self.imagenet_size, self.imagenet_size]),
                transforms.ToTensor(),
                self.normalize,
            ]
        )
        val_dataset = datasets.ImageFolder("../val", transform=transform_val)
        valid_loader = torch.utils.data.DataLoader(
            val_dataset,
            batch_size=self.batch_size,
            num_workers=self.num_workers,
            pin_memory=self.pin_memory,
        )
        return valid_loader

    def test_dataloader(self):
        transform_val = transforms.Compose(
            [
                square_pad,
                transforms.Resize([self.imagenet_size, self.imagenet_size]),
                transforms.ToTensor(),
                self.normalize,
            ]
        )
        test_dataset = datasets.ImageFolder("val", transform=transform_val)
        test_loader = torch.utils.data.DataLoader(
            val_dataset,
            batch_size=self.batch_size,
            num_workers=self.num_workers,
            pin_memory=self.pin_memory,
        )
        return test_loader

    def configure_optimizers(self):

        params_in_where = []
        params_in_what = []
        params_in_baseliner = []
        for name, params in self.named_parameters():
            if "where" in name or "locator" in name:
                params_in_where.append(params)
            elif "baseline" in name:
                params_in_baseliner.append(params)
            else:
                params_in_what.append(params)
        optimizer = torch.optim.Adam(
            [
                {"params": params_in_where, "lr": self.init_lr_where},
                {"params": params_in_baseliner, "lr": self.init_lr_baseliner},
                {"params": params_in_what, "lr": self.init_lr},
            ]
        )
        return optimizer

    def training_step(self, batch, batch_idx):
        x, y = batch
        logits, locs, log_pis, baselines = self.forward(x)
        predicted = torch.max(logits, 1)[1]
        R = (predicted.detach() == y).float()
        R = R.unsqueeze(1).repeat(1, self.num_glimpses - 1)

        loss_action = F.cross_entropy(logits, y,)
        loss_baseline = F.mse_loss(baselines, R)
        # compute reinforce loss
        adjusted_reward = R - baselines.detach()

        loss_reinforce = torch.sum(-log_pis * adjusted_reward, dim=1)
        loss_reinforce = torch.mean(loss_reinforce, dim=0)
        loss = loss_action + loss_baseline + loss_reinforce
        correct = (predicted == y).float()
        acc = correct.sum() / len(y)
        return {
            "loss": loss,
            "log": {
                "loss_action": loss_action,
                "acc": acc,
                "loss_baseline": loss_baseline,
                "loss_reinforce": loss_reinforce,
                "loss": loss,
            },
        }

    def validation_step(self, batch, batch_idx):
        x, y = batch
        logits = self.forward(x)
        predicted = torch.max(logits, 1)[1]
        correct = (predicted == y).float()
        return {"correct": correct}

    def test_step(self, batch, batch_idx):
        x, y = batch
        log_probas = self.forward(x)
        predicted = torch.max(log_probas, 1)[1]
        correct = (predicted == y).float()
        return {"correct": correct}

    def validation_epoch_end(self, outputs):
        correct_all = torch.stack([torch.sum(output["correct"]) for output in outputs])
        correct_all = torch.sum(correct_all)
        data_point_len = np.sum([len(output["correct"]) for output in outputs])
        accuracy = correct_all / data_point_len
        # for module in self.modules():
        #    if isinstance(module, torch.nn.modules.batchnorm.BatchNorm1d):
        #        module.momentum *= 0.8
        # if len(self.num_glimpses_list) > self.current_epoch:
        #    self.num_glimpses = self.num_glimpses_list[self.current_epoch]
        return {"log": {"accuracy": accuracy}}


if __name__ == "__main__":
    model = Net()
    logger = pl.loggers.TensorBoardLogger("./logging", name="imagenet_model")
    checkpoint_callback = pl.callbacks.ModelCheckpoint(
        'ckpt', monitor="accuracy", save_last=True, save_top_k=3, mode="max", verbose=True,
    )
    trainer = pl.Trainer(
        gpus=2,
        logger=logger,
        row_log_interval=50,
        checkpoint_callback=checkpoint_callback,
        log_gpu_memory=True
    )
    trainer.fit(model)
